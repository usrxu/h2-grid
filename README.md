# First steps of hydrogen grid optimization

This is the GitLab repository for my bachelor's thesis "First steps of hydrogen grid optimization
utilizing cost rasters and least-cost path
methods" and provides the Python Notebooks used for implementation, with more detailed explanations than in the thesis itself.

For the TOC links to work the Notebooks have to be opened in a Jupyter instance or an online viewer like https://nbviewer.org, GitLab doesn't support them.


## License
For open source projects, say how it is licensed.


